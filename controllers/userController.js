const User = require("../models/user.js");
const Course = require("../models/course.js")
const bcrypt = require("bcrypt");
const auth = require('../auth.js');

module.exports.checkEmailExist = (req, res) => {

	return User.find({email: req.body.email}).then(result => {
		if (result.length > 0) {
			return res.send(true);
		} else {
			return res.send(false);
		}
	});


}



module.exports.registerUser = (req, res) => {

	const reqBody = req.body

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		// bcrypt  package for password hasing
		// hashSync - syncronously generate a hash
		// hash - asynchronously generate a hash
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
        isAdmin: reqBody.isAdmin
	});

	return newUser.save().then((user, error) => {
		if (error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})


}


module.exports.loginUser = (req, res) => {
	const reqBody = req.body
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return res.send(false);
		} else {
			// compareSync is bcrypt method to compare an unhashed password with hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return res.send({access: auth.createAccessToken(result)});
			} else {
				return res.send(false);
			}
		}
	});
}

module.exports.getUserDetails = (req, res) => {
	const reqBody = req.body

	return User.findOne({_id: reqBody.id}).then(result => {
		if (result == null) {
			return res.send(false);
		} else {
			result.password = '';

			return res.send(result);
		}
	})
}


// Enroll user to a class

module.exports.enroll = async (req, res) => {

    const userId = req.body.userId;
    const courseId = req.body.courseId;

    if (!userId || !courseId) {
        return res.send('Missing values');
    }

    let isUserUpdated = await User.findById(userId).then(user => {
        user.enrollments.push({courseId: courseId});

        return user.save().then((user, err) => {
            if (err) {
                return false;
            } else {
                return true;
            }
        })
    })


    let isCourseUpdated = await Course.findById(courseId).then(course => {
        course.enrollees.push({userId: userId});

        return course.save().then((course, err) => {
            if (err) {
                return false;
            } else {
                return true;
            }
        });
    });



    if (isUserUpdated && isCourseUpdated) {
        return res.send(true);
    } else {
        return res.send(false);
    }


}