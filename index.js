const express = require('express'); 
const mongoose = require('mongoose');
const cors = require('cors'); 

const jswt = require('jsonwebtoken');

const path = require('path');

const dotenv = require('dotenv');
dotenv.config();
const dbURL = process.env.DATABASE_URL;

const port = process.env.PORT || 4000;

// to create a server via express
const app = express();

const userRouter = require('./routes/userRoute.js')
const courseRouter = require('./routes/courseRoute.js');

// Add middleware

// To allow cross origin resource sharing
app.use(cors());

// To read json objects
app.use(express.json());

// to read forms
app.use(express.urlencoded({extended: true }));


mongoose.connect(dbURL,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

// Propmpts a message once connected
mongoose.connection.once('open', () => console.log(`Now connected to Dela Rosa-Mongo DB Atlas.`));

app.use("/users", userRouter);
app.use("/courses", courseRouter);



app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});