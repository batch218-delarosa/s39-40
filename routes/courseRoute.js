const express = require("express");
const courseRouter = express.Router();

const courseController = require('../controllers/courseController.js');

const auth = require('../auth.js');


// Limit this to authenticated admin
courseRouter.post('/create', auth.verify, courseController.verifyAdmin, courseController.addCourse)

// Create route for de-activation of a course limited to admin user
// return Course.isActive

courseRouter.get('/all', courseController.getAllCourses)

courseRouter.get('/active', courseController.getActiveCourses)

courseRouter.get('/:id', courseController.getCourseById)

courseRouter.patch('/:id/update', auth.verify,courseController.verifyAdmin, courseController.updateCourse);

courseRouter.patch('/:id/archive', auth.verify,courseController.verifyAdmin, courseController.archiveCourse);



module.exports = courseRouter;